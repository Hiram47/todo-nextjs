# Development
pasos para levantar la app en desarrollo

1._ Levantar la base de datos
```
docker compose up -d
```
2._ Renombrar el .env.template a .env y hacer copia
3._ Remplazar las variables de entorno
4 ejecutar el comando ``` npm install ```
5 ejecutar el comando ``` npm run dev ```
6 ejecutar estos comandos de prisma
```
npx prisma migrate dev
npx prisma generate

```
7._ Ejecutar el SEED para [crear la BD local] (localhost:3000/api/seed)

# Prisma commands
```
npx prisma init
npx prisma migrate dev
npx prisma generate

```