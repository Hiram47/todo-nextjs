import prisma from '@/app/lib/prisma'
import { log } from 'console';
import { NextResponse } from "next/server";

export async function GET(request: Request) {

    // const todo = await prisma.todo.create({
    //     data:{description:'Piedra del alma'}
    // })

    // console.log(todo);

    await prisma.todo.deleteMany()
    
    await prisma.todo.createMany({
        data:[
            {description:'Piedra del alma',complete:true},
            {description:'Piedra del poder'},
            {description:'Piedra del tiempo'},
            {description:'Piedra del espacio'},
            {description:'Piedra del realidad'}
        ]
    })
    
    return NextResponse.json({ message: 'Seed Executed' });
}