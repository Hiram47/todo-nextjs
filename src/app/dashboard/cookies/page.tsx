import { TabBar } from "@/app/components"
import { cookies } from "next/headers"

export const metadata={
    title:'Cookie Page',
    description:'SEO title'
}

const CookiesPage = () => {
    const cookieStore=cookies()
    const cookieTab= cookieStore.get('selectedTab')?.value ?? '1'
    console.log(cookieTab);
    


  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 gap-3">
        <div className="flex flex-col">

            <span className="text-3xl" >Tabs</span>
            <TabBar currentTab={+cookieTab}/>
        </div>

    </div>
  )
}

export default CookiesPage
